﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Xml.Linq;

namespace XmlToJson.Util
{
    public class XmlToDynamic
    {
        public static void Parse(dynamic parent, XElement node)
        {
            if (node.HasElements)
            {

                if (node.Elements(node.Elements().First().Name.LocalName).Count() > 1)
                {
                    //list
                    var item = new ExpandoObject();
                    

                    var list = new List<dynamic>();
                    foreach (var element in node.Elements())
                    {
                        Parse(list, element);
                    }

                    AddProperty(item, node.Elements().First().Name.LocalName, list);
                    AddProperty(parent, node.Name.ToString(), item);
                }
                else
                {
                    var item = new ExpandoObject();

                    foreach (var attribute in node.Attributes())
                    {
                        AddProperty(item, attribute.Name.ToString(), attribute.Value.Trim());
                    }

                    //element
                    foreach (var element in node.Elements())
                    {
                        Parse(item, element);
                    }

                    AddProperty(parent, node.Name.ToString(), item);
                }
            }
            else
            {
                foreach (var attribute in node.Attributes())
                {
                    AddProperty(parent, attribute.Name.ToString(), attribute.Value.Trim());
                }
                AddProperty(parent, node.Name.ToString(), node.Value.Trim());
            }
        }

        private static void AddProperty(dynamic parent, string name, object value)
        {
            if (parent is List<dynamic>)
            {
                (parent as List<dynamic>).Add(value);
            }
            else
            {
                if ((parent as IDictionary<String, object>).ContainsKey(name))
                {
                    if ((parent as IDictionary<String, object>)[name] is List<object>)
                    {
                        ((parent as IDictionary<String, object>)[name] as List<object>).Add(value);
                    }
                    else
                    {
                        var list = new List<object>();
                        list.Add((parent as IDictionary<String, object>)[name]);
                        (parent as IDictionary<String, object>)[name] = list;
                    }
                }
                else
                {
                    (parent as IDictionary<String, object>)[name] = value;
                }
            }
        }
    }
}