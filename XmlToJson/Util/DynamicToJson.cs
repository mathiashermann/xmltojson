﻿namespace XmlToJson.Util
{
    public class DynamicToJson
    {
        public static string Parse(dynamic input)
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(input);
            return json;
        } 
    }
}