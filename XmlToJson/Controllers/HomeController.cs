﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Xml.Linq;
using XmlToJson.Util;

namespace XmlToJson.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            var model = new HomeIndex();
            return View(model);
        }


        public ActionResult Convert(HomeIndex model)
        {
            var client = new WebClient();
            client.Encoding = Encoding.GetEncoding(1252);
            var stream = client.OpenRead(model.Url);
            StreamReader reader = new StreamReader(stream);
            string xml = reader.ReadToEnd();
            dynamic root = new ExpandoObject();
            var xDoc = XDocument.Parse(xml);
            XmlToDynamic.Parse(root, xDoc.Elements().First());
            var json = DynamicToJson.Parse(root);
            json = JsonHelper.FormatJson(json);
            var convertedDataModel = new ConvertedDataModel();
            convertedDataModel.Json = json;
            convertedDataModel.Url = model.Url;
            return View(convertedDataModel);
        }

        [AllowCrossSiteJson]
        [OutputCache(Duration = 600,Location = OutputCacheLocation.ServerAndClient,VaryByParam = "url")]
        public ActionResult Permanent(string url)
        {
            var client = new WebClient();
            client.Encoding = Encoding.GetEncoding(1252);
            var stream = client.OpenRead(url);
            StreamReader reader = new StreamReader(stream);
            string xml = reader.ReadToEnd();
            //var xml = client.DownloadString(url);
            dynamic root = new ExpandoObject();
            var xDoc = XDocument.Parse(xml);
            XmlToDynamic.Parse(root, xDoc.Elements().First());
            var json = DynamicToJson.Parse(root);
            json = JsonHelper.FormatJson(json);
            return Content(json, "application/json");
        }

    }
    public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
            base.OnActionExecuting(filterContext);
        }
    }

    public class ConvertedDataModel
    {
        public string Json { get; set; }
        public string Url { get; set; }
    }

    class JsonHelper
    {
        private const string INDENT_STRING = "    ";
        public static string FormatJson(string str)
        {
            var indent = 0;
            var quoted = false;
            var sb = new StringBuilder();
            for (var i = 0; i < str.Length; i++)
            {
                var ch = str[i];
                switch (ch)
                {
                    case '{':
                    case '[':
                        sb.Append(ch);
                        if (!quoted)
                        {
                            sb.AppendLine();
                            Enumerable.Range(0, ++indent).ForEach(item => sb.Append(INDENT_STRING));
                        }
                        break;
                    case '}':
                    case ']':
                        if (!quoted)
                        {
                            sb.AppendLine();
                            Enumerable.Range(0, --indent).ForEach(item => sb.Append(INDENT_STRING));
                        }
                        sb.Append(ch);
                        break;
                    case '"':
                        sb.Append(ch);
                        bool escaped = false;
                        var index = i;
                        while (index > 0 && str[--index] == '\\')
                            escaped = !escaped;
                        if (!escaped)
                            quoted = !quoted;
                        break;
                    case ',':
                        sb.Append(ch);
                        if (!quoted)
                        {
                            sb.AppendLine();
                            Enumerable.Range(0, indent).ForEach(item => sb.Append(INDENT_STRING));
                        }
                        break;
                    case ':':
                        sb.Append(ch);
                        if (!quoted)
                            sb.Append(" ");
                        break;
                    default:
                        sb.Append(ch);
                        break;
                }
            }
            return sb.ToString();
        }
    }

    static class Extensions
    {
        public static void ForEach<T>(this IEnumerable<T> ie, Action<T> action)
        {
            foreach (var i in ie)
            {
                action(i);
            }
        }
    }

    public class HomeIndex
    {
        public string Url { get; set; }
    }
}
